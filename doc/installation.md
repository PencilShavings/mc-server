# Installing Python & Depencencys

### Windows

I recommend installing Python 2 using Chocolatey.
See [chocolatey.org](https://chocolatey.org/) for installation instructions (Spoiler its a one line copy and paste).

Once Chocolatey has been installed run:
```bash
choco install -y python2
```

### MacOS

While MacOS already ships with Python 2, I highly recommend installing Homebrews Python. This will protect you from future upgrades (MacOS likes to reset everything from python back to "original" after every sytem upgrade)
To install Homebrew see the [Homebrew website](http://brew.sh/).

Once Homebrew is installed run:
```bash
brew install python
```

### Linux

Most major Linux distros should already come with Python installed. However some distros are now shipping Python 3 as default.

If so install Python 2 via you package manager (example using Fedora & dnf)
```bash
sudo dnf install -y python2 python2-pip
```

### Dependencys

Now that Python 2 is installed we need to install some dependecys . This can be done using pip (If you are a Linux user with Python 3 also installed use pip2)
```bash
pip install --user osutil click
```

### wget

While wget is not required to use mcsvr, it is a handy tool for downloading it.

```bash
choco/brew/dnf install -y wget
```

# Downloading & Installing mcsvr

### Downloading

The easyest way to download is using wget. 
```bash
wget -N https://gitlab.com/PencilShavings/mc-server/raw/master/mcsvr.py
```
This should save mcsvr.py into your $HOME directory.

NOTE: Windows users, do NOT use an elivated command prompt (Run as Administrator) unless you cd out of the C:/Windows/System32 folder.

### Installing

Because mcsvr is a python script there really is no "installing" it. Just put it somewhere you can find it and your good.

On Windows I recomend leaving it your $HOME directory. This is where the cmd/powershell promt defaults to open.

For Mac users I would put it in /usr/local/bin. Once it is there you can access it from anywhere.

On Linux in ether /usr/local/bin for system wide acess or $HOME/.local/bin for just the local user.

Now that it is "installed" run the following to get started.

```bash
python mcsvr.py --help
```

## Getting Fancy (MacOS & Linux only)

On MacOS & Linux you can go the extra mile by making it look like a traditional cli application. All you have to do is make the file executable and then remove the .py extention.

```bash
chmod +x mcsvr.py
mv mcsvr.py mcsvr
```
This is the differece
```bash
python mcsvr.py --help
mcsvr --help
```
