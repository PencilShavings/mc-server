## mcsvr
A CLI Minecraft server like launcher

### Requierments
* Python 2
* python-osutil
* python-click

### Usage
```bash
mcsvr create "World 01"
mcsvr set_ram "World 01" --max-ram 2048 --min-ram 1024
mcsvr start "World 01"
```

Quotation marks ("" or '') are required if your server name is going to have spaces.