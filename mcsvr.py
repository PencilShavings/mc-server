#!/usr/bin/env python
"""A Minecraft Server CLI Launcher"""

__author__ = 'PencilShavings'
__version__ = '0.1'
_app_ = 'mcsvr'
_file_ = __file__.rpartition('/')[-1]

import osutil
import urllib
import json
import subprocess
from distutils.version import StrictVersion
import click

mcsvr_dir = osutil.getenv_home() + '/.mcsvr/'
version_dir = mcsvr_dir + 'versions/'
server_dir = mcsvr_dir + 'servers/'
infofile = 'mcsvr.info'

def get_versions():

	url = 'https://gitlab.com/PencilShavings/mc-server/raw/master/versions.txt'
	msg = 'Downloading the new version manifest...'
	local_file = mcsvr_dir + 'versions.txt'

	# if versions.txt does not exist, download it.
	if not osutil.file_exists(local_file):
		print msg
		urllib.urlretrieve(url, local_file)

	# Get sha1 of the local & remote versions.txt
	local_hash = osutil.get_hash(osutil.cat(local_file))
	remote_hash = osutil.get_hash(osutil.cat(url, isurl=True))

	# Compare local & remote sha1
	if local_hash != remote_hash:
		print msg
		urllib.urlretrieve(url, local_file)

	# Return the list
	return osutil.cat(local_file, aslist=True)


versions = get_versions()

xversions = ['1.2.4', '1.2.3', '1.2.2', '1.2.1', '1.1', '1.0']


# def isEmpty(string):
# 	if string == '' or string.isspace():
# 		return True
# 	else:
# 		return False

def chmodplusx(target):
	import stat

	st = osutil.os.stat(target)
	osutil.os.chmod(target, st.st_mode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)


def validMCserver(name):

	server = server_dir + name + '/'

	if not osutil.does_this_exist(server + infofile):
		print 'Server "' + name + '" not found.'
		exit()

def validateMCversion(version):
	"""Checks to see if $VERSION is valid or not."""

	if version in xversions:
		print 'While ' + version + ' is a valid Minecraft version, there is no server for it.'
		exit()
	elif version not in versions:
		print '"' + version + '" is not a valid Minecraft version.'
		exit()

	return True


@click.group()
def cli_ui():
	pass

@cli_ui.command()
@click.argument('name')
@click.option('--version', help='Desiered MC version.')
@click.pass_context
def create(ctx, name, version):
	"""Create a new Minecraft Server."""

	server = server_dir + name + '/'

	# Check if server directory exists, if not create it.
	if osutil.dir_exists(server):
		print '"' + name + '" already exits.'
		exit()
	else:
		osutil.mkdir(server, verbose=True, msg='Creating: ' + name)

	# Check if a version was specified. If not set it to the most resent.
	if version is None:
		version = versions[0]

	# Download minecraft_server.$VERSION.jar
	ctx.invoke(download, version=version)

	# Create server.properties
	# mcsvr will not create the sever.properties because it changes from version to version.

	# Create mcsvr.info
	print 'Creating: mcsvr.info'
	osutil.echo(json.dumps({'Name': name, 'Version': version, 'Max Ram': '1024M', 'Min Ram': '1024M', 'GUI': False}),
				dst=server + infofile)

@cli_ui.command()
@click.argument('version')
def download(version):
	"""Download minecrat_server.$VERSION.jar"""

	jarurl = 'https://s3.amazonaws.com/Minecraft.Download/versions/' + version + '/minecraft_server.' + version + '.jar'
	jarfile = str(jarurl.rpartition('/')[-1])

	validateMCversion(version)

	if not osutil.file_exists(version_dir + jarfile):
		print 'Downloading minecraft_server.' + version + '.jar'
		urllib.urlretrieve(jarurl, version_dir + jarfile)
	else:
		print jarfile + ' has already been downloaded...'

@cli_ui.command()
@click.argument('name')
def start(name):
	"""Starts a Server"""

	server = server_dir + name + '/'

	# Check if mcsvr.info exists.
	validMCserver(name)

	# Read mssvr.info's info.
	data = json.loads(osutil.cat(server + infofile))

	# Generate start.sh
	maxram = '-Xmx' + data['Max Ram']
	minram = '-Xms' + data['Min Ram']
	jarfile = '-jar ' + version_dir + 'minecraft_server.' + data['Version'] + '.jar'

	cdcmd = 'cd "' + server + '"'
	javacmd = 'java ' + maxram + ' ' + minram + ' ' + jarfile

	if not data['GUI']:
		javacmd += ' nogui'

	cmd = cdcmd + '\n' + javacmd

	# TODO: Fix PowerShell issue for Windows 10 Redstone Update 2
	# The Redstone 2 Update will use PowerShell by default.
	if osutil.system() == 'Windows':
		cmd = '@echo off' + '\n' + cmd
		exe = 'bat'
	else:
		cmd = '#!/bin/bash' + '\n' + cmd
		exe = 'sh'

	# "Save" start.sh
	osutil.echo(cmd, dst=server + 'start.' + exe)

	# Make start.sh executable
	chmodplusx(server + 'start.' + exe)

	# "Accept the EULA"
	if not osutil.file_exists(server + 'eula.txt'):
		osutil.echo('eula=true', dst=server + 'eula.txt')

	# Check if Java is installed
	if osutil.system() == 'Windows':
		javapath = 'C:/WINDOWS/System32/java.exe'
	elif osutil.system() == 'MacOS':
		javapath = '/usr/local/bin/java'
	else:
		javapath = '/usr/bin/java'

	if not osutil.file_exists(javapath):
		print 'Java not found.'
		exit()

	# Run start.sh
	print 'Starting Server: ' + name

	subprocess.call([server + 'start.' + exe])

@cli_ui.command()
@click.argument('name')
@click.option('--version', help='Desiered MC version. (Default: ' + versions[0] +')')
@click.pass_context
def update(ctx, name, version):
	"""Update an existing servers version."""

	server = server_dir + name + '/'

	# Check if mcsvr.info exists.
	validMCserver(name)

	# Check if a version was specified. If not set it to the most resent.
	if version is None:
		version = versions[0]

	# Check if $VERSION is valid
	validateMCversion(version)

	# Read mssvr.info's info.
	data = json.loads(osutil.cat(server + infofile))

	# Update Version
	oldversion = data['Version']
	newversion = version

	# Check if $VERSION is the same
	if newversion == oldversion:
		print 'Version is already set to ' + version
		exit()

	# Check if trying to downgrade
	if not StrictVersion(newversion) > StrictVersion(oldversion):
		print '[WARNING] Downgrading is not supported!!'
		exit()

	# Download minecraft_server.$VERSION.jar
	ctx.invoke(download, version=version)

	# Save mcsvr.info
	data['Version'] = version
	osutil.echo(json.dumps(data), dst=server + infofile)

	print 'Updated ' + name + ' from ' + oldversion + ' to ' + data['Version']

@cli_ui.command()
@click.argument('name')
@click.option('--max-ram', help='Set the max amout of RAM in megabytes.', type=int)
@click.option('--min-ram', help='Set the min amout of RAM in megabytes.', type=int)
def set_ram(name, max_ram, min_ram):
	"""Sets the Minimum & Maximum amount of RAM."""

	server = server_dir + name + '/'

	# Read mssvr.info's info.
	data = json.loads(osutil.cat(server + infofile))

	# Update Max Ram if not empty
	if max_ram is not None:
		data['Max Ram'] = str(max_ram) + 'M'

	if min_ram is not None:
		data['Min Ram'] = str(min_ram) + 'M'

	# If Max RAM is smaller than Min RAM error out
	if int(data['Max Ram'][:-1]) < int(data['Min Ram'][:-1]):
		print '[WARNING] The maximum amount of ram cannot be smaller than the minimum!!'
		exit()

	# Save mcsvr.info
	osutil.echo(json.dumps(data), dst=server + infofile)

	if max_ram is not None:
		print 'Max RAM changed to: ' + data['Max Ram']

	if min_ram is not None:
		print 'Min RAM changed to: ' + data['Min Ram']

@cli_ui.command()
def ls():
	"""List avalable Servers"""

	for server in osutil.ls(server_dir):
		if osutil.file_exists(server_dir + server + '/' + infofile):
			print server


if __name__ == '__main__':
	if not osutil.dir_exists(mcsvr_dir):
		osutil.mkdir(mcsvr_dir)
	if not osutil.dir_exists(version_dir):
		osutil.mkdir(version_dir)
	if not osutil.dir_exists(server_dir):
		osutil.mkdir(server_dir)

	cli_ui()

